import React from "react";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CardTitle from "react-bootstrap/esm/CardTitle";
import CardSubtitle from "react-bootstrap/esm/CardSubtitle";
import CardBody from "react-bootstrap/esm/CardBody";
function CardComponent({ movies }) {
  return (
    <Container>
      <Row className="d-flex mt-3" style={{width:"100%", justifyContent:"space-evenly"}}>
        {movies !== undefined ? (
          movies.map((movie) => (
                
              <Card style={{height:"52vh", width:"300px", margin:"7px"}}>
                <CardBody >
                  <CardTitle style={{fontSize:"2vh"}}>{movie.Title}</CardTitle>
                  <CardSubtitle className="mb-2 text-muted" style={{fontSize:"1.5vh"}}>
                    {movie.Year}
                  </CardSubtitle>
                </CardBody>
                <img
                  alt="Card cap"
                  src={movie.Poster}
                  width="100%"
                  height="78%"
                />
              </Card>
          
          ))
        ) :   (
          <h3>No movies found</h3>
        )}
      </Row>
    </Container>
  );
}

export default CardComponent;
