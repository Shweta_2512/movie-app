import React from "react";
import { useState } from "react";
import CardComponent from "./CardComponent";
import { useEffect } from "react";
import axios from "axios";
import Container from "react-bootstrap/esm/Container";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import SpeechRecognition, {
  useSpeechRecognition,
} from "react-speech-recognition";

function MainComponent() {

  const [movie, setMovie] = useState([]);
  const [search, setSearch] = useState("");

  const commands = [
    {
      command: "*",
      callback: () => setSearch(transcript),
    },
  ];

  const { transcript } = useSpeechRecognition({ commands });
  useEffect(() => {
    axios
      .get(`http://www.omdbapi.com/?s=${search}&apikey=f056e2f7`)
      .then((response) => {
        setMovie(response.data.Search);
        // console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [search]);
  const handleInputChange = (event) => {
    setSearch(event.target.value);
  };
  return (
    <Container>
      <Row className="mt-5">
        <Col></Col>
        <Col sm={5} style={{ display: "flex" }}>
          <input
            type="text"
            value={search}
            onChange={handleInputChange}
            placeholder="Search..."
            className="form-control"
          />
          &nbsp;
          <button
            className="btn btn-primary"
            onClick={SpeechRecognition.startListening}
          >
            <i class="fa-solid fa-microphone"></i>
          </button>
        </Col>
        <Col></Col>
      </Row>
      <CardComponent movies={movie} />
    </Container>
  );
}

export default MainComponent;
